﻿using GersonFrame.ABFrame;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GersonFrame
{
    public abstract partial class MonoBehaviourSimplify : MonoBehaviour
    {
        #region Delay
        public void Delay(float seconds, Action onFinished)
        {
            if (gameObject.activeInHierarchy)
            {
                StartCoroutine(DelayCoroutine(seconds, onFinished));
            }
        }

        public void Delay(float seconds,Action<object> onFinishe,object param=null)
        {
            if (gameObject.activeInHierarchy)
            {
                StartCoroutine(DelayCoroutine(seconds, onFinishe));
            }
        }

        private IEnumerator DelayCoroutine(float seconds, Action onFinished)
        {
            yield return new WaitForSeconds(seconds);
            onFinished();
        }

        private IEnumerator DelayCoroutine(float seconds, Action<object> onFinished, object param = null)
        {
            yield return new WaitForSeconds(seconds);
            onFinished(param);
        }
        #endregion

        #region MsgDispatcher
        List<MsgRecord> mMsgRecorder = new List<MsgRecord>();

        class MsgRecord
        {
            private MsgRecord() { }

            static Stack<MsgRecord> mMsgRecordPool = new Stack<MsgRecord>();



            /// <summary>
            /// 根据字符串获得消息对象
            /// </summary>
            /// <param name="msgName"></param>
            /// <param name="onMsgReceived"></param>
            /// <returns></returns>
            public static MsgRecord Allocate(string msgName, Action<object> onMsgReceived)
            {
                var retRecord = mMsgRecordPool.Count > 0 ? mMsgRecordPool.Pop() : new MsgRecord();
                retRecord.MsgName = msgName;
                retRecord.OnMsgReceived = onMsgReceived;
                return retRecord;
            }

            public void Recycle()
            {
                MsgName = null;
                OnMsgReceived = null;
                mMsgRecordPool.Push(this);
            }

            public string MsgName = null;
            public Action<object> OnMsgReceived;

        }

 

        /// <summary>
        /// 根据字符串注册消息
        /// </summary>
        /// <param name="msgName"></param>
        /// <param name="onMsgReceived"></param>
        public void RegisterMsg(string msgName, Action<object> onMsgReceived)
        {
            MsgDispatcher.Register(msgName, onMsgReceived);
            mMsgRecorder.Add(MsgRecord.Allocate(msgName, onMsgReceived));
        }
        


        /// <summary>
        /// 根据字符串发送消息
        /// </summary>
        /// <param name="msgName"></param>
        /// <param name="data"></param>
        public void SendMsg(string msgName, object data = null)
        {
            MsgDispatcher.Send(msgName, data);
        }






        /// <summary>
        /// 根据字符串卸载所有消息
        /// </summary>
        /// <param name="msgName"></param>
        public void UnRegisterMsg(string msgName)
        {
            if (string.IsNullOrEmpty(msgName))
                return;
            var selectedRecords = mMsgRecorder.FindAll(record => record.MsgName == msgName);
            selectedRecords.ForEach(record =>
            {
                MsgDispatcher.UnRegister(record.MsgName, record.OnMsgReceived);
                mMsgRecorder.Remove(record);
                record.Recycle();
            });
            selectedRecords.Clear();
        }

        /// <summary>
        /// 根据字符串卸载指定消息
        /// </summary>
        /// <param name="msgName"></param>
        /// <param name="onMsgReceived"></param>
        public void UnRegisterMsg(string msgName, Action<object> onMsgReceived)
        {
            if (string.IsNullOrEmpty(msgName))
                return;
            var selectedRecords = mMsgRecorder.FindAll(record => record.MsgName == msgName && record.OnMsgReceived == onMsgReceived);
            selectedRecords.ForEach(record =>
            {
                MsgDispatcher.UnRegister(record.MsgName, record.OnMsgReceived);
                mMsgRecorder.Remove(record);
                record.Recycle();
            });
            selectedRecords.Clear();
        }


        protected virtual void OnDestroy()
        {
            OnBeforeDestroy();
            UnRegisterAll();
        }

        public  void UnRegisterAll()
        {
            foreach (var msgRecord in mMsgRecorder)
            {
                MsgDispatcher.UnRegister(msgRecord.MsgName, msgRecord.OnMsgReceived);
                msgRecord.Recycle();
            }
            mMsgRecorder.Clear();
        }

        protected abstract void OnBeforeDestroy();

        #endregion

        #region ObjectManager
       public virtual void Recycle(object param=null)
        {
            this.gameObject.Hide();
            ObjectManager.Instance.ReleaseObject(this.gameObject);
        }
        #endregion

    }

}