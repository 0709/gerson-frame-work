﻿using UnityEngine;
using System.Collections;


namespace GersonFrame.Tool
{

    public class FPSCounter : MonoBehaviour
    {

        private float currentTime = 0;
        private float lateTime = 0;

        private float framesNum = 0;
        private float fpsTime = 0;

        GUIStyle style = new GUIStyle();


        private void Start()
        {
            style.fontSize = 50;
        }

        // Update is called once per frame
        void Update()
        {
            currentTime += Time.deltaTime;

            framesNum++;

            if (currentTime - lateTime >= 1.0f)
            {
                fpsTime = framesNum / (currentTime - lateTime);

                lateTime = currentTime;

                framesNum = 0;
            }
        }



        void OnGUI()
        {
            GUI.Label(new Rect(0, 0, 300, 300), "fps:" + fpsTime.ToString(), style);
        }
    }
}
