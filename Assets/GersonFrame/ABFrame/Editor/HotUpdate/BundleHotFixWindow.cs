﻿using UnityEngine;
using UnityEditor;
using System.Runtime.InteropServices;


namespace GersonFrame
{


    public class BundleHotFixWindow : EditorWindow
    {
        string md5Path = "";
        string hotCunt = "1";
        OpenFileName m_openfileName = null;
        [MenuItem("MyTools/热更/打开热更包窗口")]
        static void Init()
        {
            BundleHotFixWindow window = EditorWindow.GetWindow<BundleHotFixWindow>(false, "热更包窗口(选择的ABMd5配置文件进行打包)", true);
            window.Show();
        }




        private void OnGUI()
        {
            GUILayout.BeginHorizontal();
            md5Path = EditorGUILayout.TextField("ABMD5路径", md5Path, GUILayout.Width(350), GUILayout.Height(20));
            if (GUILayout.Button("选择ABMD5版本文件", GUILayout.Width(150), GUILayout.Height(30)))
            {
                m_openfileName = new OpenFileName();
                m_openfileName.structSize = Marshal.SizeOf(m_openfileName);
                m_openfileName.filter = "ABMD5文件(*.bytes)\0*.bytes";
                m_openfileName.file = new string(new char[256]);
                m_openfileName.maxFile = m_openfileName.file.Length;
                m_openfileName.fileTitle = new string(new char[64]);
                m_openfileName.maxFileTitle = m_openfileName.fileTitle.Length;
                m_openfileName.initialDir = (Application.dataPath + "/../Version").Replace("/", "\\");//默认打开路径
                m_openfileName.title = "请选择对应版本的MD5文件";
                m_openfileName.flags = 0x00080000 | 0x00001000 | 0x00000800 | 0x00000008;

                if (LocalDialog.GetSaveFileName(m_openfileName))
                {
                    Debug.Log(m_openfileName.file);
                    md5Path = m_openfileName.file;
                }
            }
            GUILayout.EndHorizontal();
            //============================
            GUILayout.BeginVertical();
            hotCunt = EditorGUILayout.TextField("热更补丁版本(第几次热更)", hotCunt, GUILayout.Width(350), GUILayout.Height(20));
            GUILayout.EndVertical();
            if (GUILayout.Button("更新热更包", GUILayout.Width(100), GUILayout.Height(50)))
            {
                if (!string.IsNullOrEmpty(md5Path) && md5Path.EndsWith(".bytes"))
                {
                    //构建热更包
                    BundleEditor.BuildAB(true, md5Path, hotCunt);
                }
                else
                    Debug.LogError("请检查文件路径:" + md5Path);
            }
        }

    }
}
