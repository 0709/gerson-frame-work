

namespace GersonFrame
{
    public abstract class BaseState
    {
        public string m_StateId;

        //为防止报错 这里先用StateController 代替 将来会是具体类
      protected  BaseState(MonoBehaviourSimplify statecontroller, string stateId)
        {
            this.m_role = statecontroller; 
            this.m_StateId = stateId;
        }

        protected MonoBehaviourSimplify m_role = null;

        /// <summary>
        /// 是否可以跳转到下一状态
        /// </summary>
        /// <param name="transition"></param>
        /// <returns></returns>
        public virtual bool CanTransition(string transition)
        {
            return true;
        }

        public abstract void OnEnter( object param1, object param2=null, object param3 = null);

        public abstract void Act();

        public abstract void LateAct();

        public abstract void onExit();

        /// <summary>
        /// 转换状态
        /// </summary>
        public abstract void UpdateTransition();

    }
}