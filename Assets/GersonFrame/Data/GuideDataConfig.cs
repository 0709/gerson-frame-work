﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GersonFrame
{
    [CreateAssetMenu(fileName = "GuideDataConfig", menuName = "CreateGuideDataConfig", order = 1007)]
    public class GuideDataConfig: ScriptableObject
    {
        [Header("新手引导配置信息")]
        public List<GuideData> GuideDataConfigList = new List<GuideData>();
    }


    [System.Serializable]
    public class GuideData
    {
        [Tooltip("步骤ID")]
        public string StepID;
        [Tooltip("名字")]
        public string Name;
        [Tooltip("下一步骤的ID")]
        public string NextStepID;
        [Tooltip("是否结束引导步骤")]
        public bool EndStep;
        [Tooltip("当前步骤显示的物体名称")]
        public string CurrentStepPrefab = "";
        [Tooltip("完成当前步骤发送的消息名称")]
        public string FinishMessage = null;
        [Tooltip("当前步骤要显示的文字信息")]
        public string[] UIText = null;
        [Tooltip("对话框显示位置")]
        public Vector2 DialogPosition;
        [Tooltip("是否是存档点")]
        public bool isCheckPoint;
        [Tooltip("是否自动下一个")]
        public bool autoNext;
    }


    public class GuideMessage
    {
        /// <summary>
        /// 开始新手引导关卡
        /// </summary>
        public const string StartGuideLevel = "StartGuideLevel";
        /// <summary>
        /// 新手引导故事结束
        /// </summary>
        public const string GuideStoryEnd = "GuideStoryEnd";
        /// <summary>
        /// 新手引导金币拾取超时
        /// </summary>
        public const string GetAllguideCoinsTimeOut = "GetAllguideCoinsTimeOut";
        /// <summary>
        /// 新手引导拾取金币结束
        /// </summary>
        public const string GetAllguideCoinsFinish = "GetAllguideCoinsFinish";
        /// <summary>
        ///新手引导提示攻击敌人
        /// </summary>
        public const string AttackEnemy = "AttackEnemy";
        /// <summary>
        /// 新手提示攻击敌人结束
        /// </summary>
        public const string AttackEnemyFinish = "AttackEnemyFinish";
        /// <summary>
        /// 显示滚动木头
        /// </summary>
        public const string ShowWood = "ShowWood";
        /// <summary>
        /// 显示突进提示对话框
        /// </summary>
        public const string ShowStickAttackTip = "ShowStickAttackTip";
        /// <summary>
        /// 突进结束
        /// </summary>
        public const string FinishStickAttack = "FinishStickAttack";
        /// <summary>
        /// 结束突进提示对话框
        /// </summary>
        public const string FinishStickAttackTip = "FinishStickAttackTip";
        /// <summary>
        /// 结束重击提示
        /// </summary>
        public const string FinishHeavyAttackTip = "FinishHeavyAttackTip";
        /// <summary>
        /// 结束重击
        /// </summary>
        public const string FinishHeavyAttack = "FinishHeavyAttack";
        /// <summary>
        /// 显示重击提示对话框
        /// </summary>
        public const string ShowHeavyAttackTip = "ShowHeavyAttackTip";
        /// <summary>
        /// 开始引导步骤
        /// </summary>
        public const string StartGuide = "StartGuide";

        /// <summary>
        /// 开始引导步骤
        /// </summary>
        public const string StartGuideSaved = "StartGuideSaved";
        /// <summary>
        /// 显示对话结束
        /// </summary>
        public const string ShowGuideFinish = "GuideDialogFinish";
        /// <summary>
        /// 显示引导对话框
        /// </summary>
        public const string ShowGuideStart = "ShowGuideStart";
        /// <summary>
        /// 结束起名引导
        /// </summary>
        public const string NameEnd = "NameEnd";
        /// <summary>
        /// 获取开始按钮位置
        /// </summary>
        public const string StartButtonPosition = "GuideGetStartButtonPosition";
        /// <summary>
        /// 获取成就按钮位置
        /// </summary>
        public const string AchievementButtonPosition = "AchievementButtonPosition";
        /// <summary>
        /// 打开成就界面引导结束
        /// </summary>
        public const string OpenAchievementEnd = "OpenAchievementEnd";
        /// <summary>
        /// 获取成就奖励引导结束
        /// </summary>
        public const string GetAchievementRewardEnd = "GetAchievementRewardEnd";
        /// <summary>
        /// 关闭成就引导结束
        /// </summary>
        public const string CloseAchievementEnd = "CloseAchievementEnd";
        /// <summary>
        /// 英雄按钮位置
        /// </summary>
        public const string HeroButtonPosition = "HeroBottonPosition";
        /// <summary>
        /// 打开英雄界面结束
        /// </summary>
        public const string OpenHeroEnd = "OpenHeroEnd";
        /// <summary>
        /// 英雄升级按钮位置
        /// </summary>
        public const string LevelUpButtonPosition = "LevelUpButtonPosition";
        /// <summary>
        /// 英雄升级结束
        /// </summary>
        public const string LevelUpHeroEnd = "LevelUpHeroEnd";
        /// <summary>
        /// 获取冒险按钮位置
        /// </summary>
        public const string AdventureBtnPosition = "AdventureBtnPosition";
        /// <summary>
        /// 返回冒险结束
        /// </summary>
        public const string ReturnAdventureEnd = "ReturnAdventureEnd";
        /// <summary>
        /// 挂机按钮位置
        /// </summary>
        public const string HangUpButtonPosition = "HangUpButtonPosition";
        /// <summary>
        /// 打开挂机界面结束
        /// </summary>
        public const string OpenHangUpEnd = "OpenHangUpEnd";
        /// <summary>
        ///获取英雄槽位1位置
        /// </summary>
        public const string HeroSlot1Position = "HeroSlot1Position";
        /// <summary>
        /// 打开选择英雄结束
        /// </summary>
        public const string OpenSeleteHeroEnd = "OpenSeleteHeroEnd";
        /// <summary>
        /// 获取选择英雄按钮的位置
        /// </summary>
        public const string HangupSelectHeroPoisition = "HangupSelectHeroPoisition";
        /// <summary>
        /// 选择挂机英雄结束
        /// </summary>
        public const string HangupSelectHeroEnd = "HangupSelectHeroEnd";
        /// <summary>
        /// 获取挂机奖励按钮位置
        /// </summary>
        public const string HangupGetRewardPosition = "HangupGetRewardPosition";
        /// <summary>
        /// 挂机奖励领取结束
        /// </summary>
        public const string HangUpGetRewardEnd = "HangUpGetRewardEnd";
        /// <summary>
        /// 获取神器按钮位置
        /// </summary>
        public const string GrailButtonPosition = "GrailButtonPosition";
        /// <summary>
        /// 打开神器引导结束
        /// </summary>
        public const string OpenGrailEnd = "OpenGrailEnd";
        /// <summary>
        /// 获取神器升级按钮位置
        /// </summary>
        public const string UpgradeGrailPosition = "UpgradeGrailPosition";
        /// <summary>
        /// 神器升级开始
        /// </summary>
        public const string UpgradeGrailStart = "UpgradeGrailStart";
        /// <summary>
        /// 神器升级结束
        /// </summary>
        public const string UpgradeGrailEnd = "UpgradeGrailEnd";
        /// <summary>
        /// 神器解锁结束
        /// </summary>
        public const string UpgradeGrail3End = "UpgradeGrail3End";
        /// <summary>
        /// 获取神器装备按钮位置
        /// </summary>
        public const string GrailEquipPosition = "GrailEquipPosition";
        /// <summary>
        /// 结束神器装备引导
        /// </summary>
        public const string EndGrailEquip = "EndGrailEquip";
        /// <summary>
        /// 家园系统按钮位置
        /// </summary>
        public const string HomelandButtonPosition = "HomelandButtonPosition";
        /// <summary>
        /// 打开家园系统结束
        /// </summary>
        public const string OpenHomelandEnd = "OpenHomelandEnd";
        /// <summary>
        /// 铁匠铺位置
        /// </summary>
        public const string SmithyButtonPosition = "SmithyButtonPosition";
        /// <summary>
        /// 打开铁匠铺结束
        /// </summary>
        public const string OpenSmithyEnd = "OpenSmithyEnd";
        /// <summary>
        /// 普通打造结束
        /// </summary>
        public const string NormalMakerPosition = "NormalMakerPosition";
        /// <summary>
        /// 结束普通打造
        /// </summary>
        public const string NormalMakeEnd = "NormalMakeEnd";
        /// <summary>
        /// 装备按钮位置
        /// </summary>
        public const string EquipmentButtonPosition = "EquipmentButtonPosition";
        /// <summary>
        /// 打开装备结束
        /// </summary>
        public const string OpenEquipmentEnd = "OpenEquipmentEnd";
        /// <summary>
        /// 装备按钮位置
        /// </summary>
        public const string EquipmentItemPosition = "EquipmenItemPosition";
        /// <summary>
        /// 打开装备详情结束
        /// </summary>
        public const string PointerEquipmentItemEnd = "PointerEquipmentItemEnd";
        /// <summary>
        /// 获取装备按钮位置
        /// </summary>
        public const string HeroEquipmentButtonPosition = "HeroEquipmentButtonPosition";
        /// <summary>
        /// 装备引导结束
        /// </summary>
        public const string HeroEquipEnd = "HeroEquipEnd";
        /// <summary>
        /// 神器剧情结束
        /// </summary>
        public const string GrailStoryEnd = "GrailStoryEnd";
        /// <summary>
        /// 打开挂机界面
        /// </summary>
        public const string OpenHangup = "OpenHangup";
    }
  



}
