﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace GersonFrame.ABFrame
{

    [CreateAssetMenu(fileName = "ABConfig", menuName = "CreateABConfig", order =1000)]
    public class ABConfig : ScriptableObject
    {
        /// <summary>
        /// 单个文件所在文件夹路径，会遍历这个文件夹下的所有prefab,所有prefab名字不能重复,必须保证名字的唯一性
        /// </summary>
        public List<string> m_PrefabsPath = new List<string>();
        /// <summary>
        /// 所有文件夹AB包
        /// </summary>
        public List<FileDirABName> m_AllFileDirAB = new List<FileDirABName>();

        [System.Serializable]//序列化结构体 字典不能使用该特性进行序列化
        public struct FileDirABName
        {
            public string ABName;
            public string Path;
        }

    }

}
