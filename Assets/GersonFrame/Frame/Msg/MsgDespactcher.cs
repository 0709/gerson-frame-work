﻿
using GersonFrame.Tool;
using System;
using System.Collections.Generic;

namespace GersonFrame
{
   
    public class MsgDispatcher
    {


        static Dictionary<string, Action<object>> mRegisteredStrMsgs = new Dictionary<string, Action<object>>();


        //==========================================================================

        public static void Register(string msgName, Action<object> onMsgReceived)
        {
            if (string.IsNullOrEmpty(msgName)) {
                MyDebuger.LogError("注册消息请传递消息名 当前消息名称为 空");
                return;
            } 
            if (!mRegisteredStrMsgs.ContainsKey(msgName))
            {
                mRegisteredStrMsgs.Add(msgName, _ => { });
            }
            mRegisteredStrMsgs[msgName] += onMsgReceived;
        }


        public static void UnRegisterAll(string msgName)
        {
            if (mRegisteredStrMsgs.ContainsKey(msgName))
                mRegisteredStrMsgs.Remove(msgName);
        }

        public static void UnRegister(string msgName, Action<object> onMsgReceived)
        {
            if (string.IsNullOrEmpty(msgName)) return;
            if (mRegisteredStrMsgs.ContainsKey(msgName))
            {
                mRegisteredStrMsgs[msgName] -= onMsgReceived;
            }
        }

        public static void Send(string msgName, object data = null)
        {
            if (mRegisteredStrMsgs.ContainsKey(msgName))
            {
                mRegisteredStrMsgs[msgName](data);
            }
        }

    }
}