﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GersonFrame
{

    public enum SonGuideStepType
    {
        ClickGesture,
        CollectCoin,
        AttackMonster,
        StickSkillUse,
        StepOver
    }

    public class BaseStep : MonoBehaviour
    {
        [Tooltip("步骤名称")]
        public SonGuideStepType m_StepType;
        [Tooltip("引导目标")]
        public RectTransform target;
        [Tooltip("引导跟随目标")]
        public RectTransform m_targetPosRect;
        [Tooltip("引导形状")]
        public GuideType m_guideType = GuideType.Rect;
        [Tooltip("引导形状缩放倍数")]
        public float m_Scal = 1;
        [Tooltip("引导形状缩放动画时间")]
        public float m_scalTime = 1;
        [Tooltip("出现模式（位移模式）")]
        public TranslateType m_translateTyep = TranslateType.Direct;
        [Tooltip("引导形位置移动动画时间")]
        public float m_TranlateTime = 1;
        [Tooltip("延迟出现时间")]
        public float m_delayTime = 0f;


        public void Show()
        {
            this.gameObject.Show();
        }

        public void Hide()
        {
            this.gameObject.Hide();
        }

        public virtual void ExcuteStep(Canvas canvas)
        {
            this.Show();
            GuideManager.Instance.Guide(canvas, this.target, this.m_guideType, m_Scal, m_scalTime, this.m_translateTyep, m_TranlateTime);
        }

        protected virtual void Update()
        {
            //赋值引导位置信息
            if (m_targetPosRect != null)
            {
                m_targetPosRect.localPosition = GuideManager.Instance.Center;
            }
        }
    }
}
