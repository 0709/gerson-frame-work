﻿
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GersonFrame.ABFrame
{


[CreateAssetMenu(fileName = "AssetLoadConfig", menuName = "CreateAssetLoadConfig", order = 1004)]
public class AssetLoadConfig : ScriptableObject
{
    [Header("资源加载模式")]
    public AssetLoadModel m_AssetLoadMode = AssetLoadModel.LoadFromEditor;
    [Header("预加载资源信息")]
    public List<PreLoadFloderInfo> m_PreLoadInfo= new List<PreLoadFloderInfo>();
}


[System.Serializable]
public struct PreLoadFloderInfo
{
    [Header("资源路径")]
    public string PreLoadFloderPath;
    [Header("预加载个数")]
    public int PreLoadCount;
}


}
