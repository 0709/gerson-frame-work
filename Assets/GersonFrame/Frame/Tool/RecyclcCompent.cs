﻿using GersonFrame.ABFrame;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace GersonFrame.Tool
{
    public class RecyclcCompent : MonoBehaviour
    {

        public void DisableSelfByTime(float time=0)
        {
            Invoke("Disable", time);
        }

        private void Disable()
        {
            ObjectManager.Instance.ReleaseObject(this.gameObject);
        }


    }
}
