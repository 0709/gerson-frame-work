namespace GersonFrame.UI
{
    interface BaseInnerView
    {
        void Show(object param = null);
        void Hide();
    }
}