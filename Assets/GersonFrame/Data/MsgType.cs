﻿

public class MsgType
{
    public const string None = "None";
        /// <summary>
        /// 游戏加载
        /// </summary>
    public const string GameLoading= "GameLoading";
        /// <summary>
        ///游戏加载结束
        /// </summary>
    public const string GameLoadOver= "GameLoadOver";
        /// <summary>
        /// 游戏开始
        /// </summary>
    public const string GameStart= "GameStart";
        /// <summary>
        /// 游戏恢复
        /// </summary>
    public const string GameResume= "GameResume";
        /// <summary>
        /// 游戏暂停
        /// </summary>
    public const string GamePause= "GamePause";
        /// <summary>
        /// 清除游戏资源
        /// </summary>
    public const string ClearGameAssets= "ClearGameAssets";
        /// <summary>
        /// 游戏结束
        /// </summary>
    public const string GameOver= "GameOver";
        /// <summary>
        /// 游戏失败
        /// </summary>
    public const string GameFail= "GameFail";
        /// <summary>
        /// 挂机开始
        /// </summary>
    public const string HangUpGameStart= "HangUpGameStart";
        /// <summary>
        /// 挂机结束
        /// </summary>
    public const string HangUpGameOver= "HangUpGameOver";
        /// <summary>
        /// 玩家操作按下
        /// </summary>
    public const string OnMouseDown= "OnMouseDown";
        /// <summary>
        /// 玩家操作抬起
        /// </summary>
    public const string OnMouseUp= "OnMouseUp";
        /// <summary>
        /// 玩家操作移动
        /// </summary>
    public const string OnMouseMove= "OnMouseMove";
        /// <summary>
        /// 下一波次动画开始
        /// </summary>
    public const string OnNextWaveAmStart= "OnNextWaveAmStart";
        /// <summary>
        /// 下一波次动画结束
        /// </summary>
    public const string OnNextWaveAmEnd= "OnNextWaveAmEnd";
        /// <summary>
        /// 游戏胜利
        /// </summary>
    public const string OnVictory= "OnVictory";
        /// <summary>
        /// 游戏失败
        /// </summary>
    public const string OnFail= "OnFail";
        /// <summary>
        /// 敌人Boss死亡
        /// </summary>
    public const string OnEnemyBossDeath= "OnEnemyBossDeath";
        /// <summary>
        /// 连击
        /// </summary>
    public const string OnHit= "OnHit";
        /// <summary>
        /// 当能量值变化时
        /// </summary>
    public const string OnEnergyRefresh= "OnEnergyRefresh";
        /// <summary>
        /// 初始化能量值
        /// </summary>
    public const string OnEnergyNotEnough= "OnEnergyNotEnough";
        /// <summary>
        /// 当玩家复活时
        /// </summary>
    public const string OnRevive= "OnRevive";
        /// <summary>
        /// 玩家角色死亡
        /// </summary>
    public const string OnPlayerDeath= "OnPlayerDeath";
        /// <summary>
        /// 当玩家获取金币
        /// </summary>
    public const string OnGetCoin= "OnGetCoin";
        /// <summary>
        /// 当玩家获取钻石时
        /// </summary>
    public const string OnGetDiamon= "OnGetDiamon";
        /// <summary>
        /// 当获取动画结束
        /// </summary>
    public const string OnGetAnimEnd= "OnGetAnimEnd";
        /// <summary>
        /// 当金币或钻石数量发生变化
        /// </summary>
    public const string OnRichesChanged= "OnRichesChanged";
        /// <summary>
        /// 当金币或钻石不足时
        /// param 金币：true，钻石：false
        /// </summary>
    public const string OnRichesNotEnough= "OnRichesNotEnough";
        /// <summary>
        /// 当体力使用或回复时
        /// </summary>
    public const string OnStrengthChanged= "OnStrengthChanged";
        /// <summary>
        /// 当体力不足时
        /// </summary>
    public const string OnStrengthNotEnough= "OnStrengthNotEnough";
        /// <summary>
        /// 震屏
        /// </summary>
    public const string ShakeCamera= "ShakeCamera";
        /// <summary>
        /// 显示过渡动画
        /// </summary>
    public const string ShowTransitionAm= "ShowTransitionAm";
        /// <summary>
        /// 关闭动画结束
        /// </summary>
    public const string ShowTransitionAmeOver= "ShowTransitionAmeOver";
        /// <summary>
        ///过渡 隐藏动画结束
        /// </summary>
    public const string HideTransitionAmeOver= "HideTransitionAmeOver";
        /// <summary>
        /// 地图加速
        /// </summary>
    public const string MapAddSpeed= "MapAddSpeed";
        /// <summary>
        /// 地图加速结束
        /// </summary>
    public const string MapAddSpeedOver= "MapAddSpeedOver";
        /// <summary>
        /// 当商店展示模型改变时
        /// </summary>
    public const string OnChangeShowMod= "OnChangeShowMod";
        /// <summary>
        /// 当改变出战角色时
        /// </summary>
    public const string OnChangeCurSelectRole= "OnChangeCurSelectRole";

        /// <summary>
        /// 当显示结算奖励动画时
        /// </summary>
    public const string OnPlayRewardAnim= "OnPlayRewardAnim";
        /// <summary>
        /// 当英雄升级时
        /// </summary>
    public const string OnUpgradeHero= "OnUpgradeHero";
        /// <summary>
        /// 当切换神器界面时
        /// </summary>
    public const string OnChangeGrail= "OnChangeGrail";
        /// <summary>
        /// 神器升级结束时刷新界面
        /// </summary>
    public const string OnGrailUpgradeEnd= "OnGrailUpgradeEnd";

        /// <summary>
        /// 重击准备完毕
        /// </summary>
    public const string HeavyAttackReady= "HeavyAttackReady";
        /// <summary>
        /// 重击没有准备完毕
        /// </summary>
    public const string HeavyAttackNotReady= "HeavyAttackNotReady";
        /// <summary>
        /// 当显示天赋提示时
        /// </summary>
    public const string OnShowGiftTips= "OnShowGiftTips";
        /// <summary>
        /// 当显示神器详细信息时
        /// </summary>
    public const string OnShowGrailTips= "OnShowGrailTips";
        /// <summary>
        /// 当购买体力时
        /// </summary>
    public const string OnBuyStrength= "OnBuyStrength";
        /// <summary>
        /// 当选择挂机英雄时
        /// </summary>
    public const string OnSelectHangUpHero= "OnSelectHangUpHero";
        /// <summary>
        /// 播放活跃度获取动画
        /// </summary>
    public const string ActivationAnim= "ActivationAnim";
        /// <summary>
        /// 当玩家更换名字时
        /// </summary>
    public const string OnNameChanged= "OnNameChanged";
        /// <summary>
        /// 显示装备的装备信息
        /// </summary>
    public const string ShowEquippedInfo= "ShowEquippedInfo";
        /// <summary>
        /// 显示当前选择的装备信息
        /// </summary>
    public const string ShowSelectedInfo= "ShowSelectedInfo";
        /// <summary>
        /// 显示成就界面的装备信息
        /// </summary>
    public const string ShowEquipmentInfoA= "ShowEquipmentInfoA";
        /// <summary>
        /// 显示装备魔盒的装备信息
        /// </summary>
    public const string ShowSelectedInfoM= "ShowSelectedInfoM";
        /// <summary>
        /// 当更换&装备 装备时
        /// </summary>
    public const string OnChangeEquipment= "OnChangeEquipment";
        /// <summary>
        /// 当脱下装备时
        /// </summary>
    public const string OnUnequipEquipment= "OnUnequipEquipment";
        /// <summary>
        /// 刷新神器技能cd时间 parm：int[2]{id="";cd}
        /// </summary>
    public const string RefreshGrailCD= "RefreshGrailCD";
        /// <summary>
        /// 获取神器技能总时长
        /// param：int[2]{id="";总时长}
        /// </summary>
    public const string GetGrailSkillCD= "GetGrailSkillCD";
    public const string UseGrailSkill= "UseGrailSkill";
        /// <summary>
        /// 当任务刷新时
        /// </summary>
    public const string OnRefreshTask= "OnRefreshTask";
        /// <summary>
        /// 刷新挂机速率
        /// </summary>
    public const string RefreshHangupRate= "RefreshHangupRate";
        /// <summary>
        /// 上阵挂机英雄
        /// </summary>
    public const string OnHangupHeroUp= "OnHangupHeroUp";
        /// <summary>
        /// 下阵挂机英雄
        /// </summary>
    public const string OnHangupHeroDown= "OnHangupHeroDown";
        /// <summary>
        /// 时光沙漏
        /// </summary>
    public const string TimeHourGlass= "TimeHourGlass";
        /// <summary>
        /// 获取成就奖励
        /// </summary>
    public const string OnGetAchievement= "OnGetAchievement";
        /// <summary>
        /// 选择被分解的装备
        /// </summary>
    public const string OnSelectDecompositionEquipment= "OnSelectDecompositionEquipment";
        /// <summary>
        /// 取消选择分解装备
        /// </summary>
    public const string OnDeselectDecompositionEquipment= "OnDeselectDecompositionEquipment";
        /// <summary>
        /// 任务跳转
        /// </summary>
    public const string TaskJump= "TaskJump";
        /// <summary>
        /// 解锁功能动画
        /// </summary>
    public const string UnlockFunctionAnim= "UnlockFunctionAnim";
        /// <summary>
        /// 挂机界面魔魂动画 param:怪物的世界坐标（Vector3)
        /// </summary>
    public const string HangUpSoulAnim= "HangUpSoulAnim";
        /// <summary>
        /// 挂机奖励魔魂动画反馈
        /// </summary>
    public const string HangUpShakeRewardImage= "HangUpShakeRewardImage";
        /// <summary>
        /// 获取英雄动画
        /// </summary>
    public const string GetHeroAnim= "GetHeroAnim";
        /// <summary>
        /// 滑动切换神器
        /// </summary>
    public const string ChangeGrailRoll= "ChangeGrailRoll";
        /// <summary>
        /// 家园界面刷新资源数量
        /// </summary>
    public const string RefreshResource= "RefreshResource";
        /// <summary>
        /// 守护者大厅获得的体力
        /// </summary>
    public const string GuradianStrength= "GuradianStrength";
        /// <summary>
        /// 交易所获得金币
        /// </summary>
    public const string ExchangeCoin= "ExchangeCoin";
        /// <summary>
        /// 奖励获得界面展示tips
        /// </summary>
    public const string RewardPanelTips= "RewardPanelTips";
        /// <summary>
        /// 改变家园进驻英雄
        /// </summary>
    public const string HomelandChangeHero= "HomelandChangeHero";
        /// <summary>
        /// 装备魔盒选择装备
        /// </summary>
    public const string MagicBoxSelectEquipment= "MagicBoxSelectEquipment";
        /// <summary>
        /// 重铸界面点击锁定
        /// </summary>
    public const string RecastLock= "RecastLock";
        /// <summary>
        /// 选择镶嵌宝石
        /// </summary>
    public const string SelectInlayGem= "SelectInlayGem";
        /// <summary>
        /// 镶嵌宝石
        /// </summary>
    public const string GemInlay= "GemInlay";
        /// <summary>
        /// 选择升星材料
        /// </summary>
    public const string SelectCostEquip= "SelectCostEquip";
        /// <summary>
        /// 装备升星时
        /// </summary>
    public const string OnEquipmentLevelUp= "OnEquipmentLevelUp";
        /// <summary>
        /// 选择合成宝石
        /// </summary>
    public const string SelectTargetGem= "SelectTargetGem";
        /// <summary>
        /// 获取装备时
        /// </summary>
    public const string GetEquipment= "GetEquipment";
        /// <summary>
        /// 家园建筑升级
        /// </summary>
    public const string HomelandBuildingUpgrade= "HomelandBuildingUpgrade";
        /// <summary>
        /// 装备魔盒显示宝石详情
        /// </summary>
    public const string ShowGemInfo= "ShowGemInfo";
        /// <summary>
        /// 击杀小怪
        /// </summary>
    public const string KillMonster= "KillMonster";
        /// <summary>
        /// 击杀boss
        /// </summary>
    public const string KillBoss= "KillBoss";
        /// <summary>
        /// 新手引导获取点击区域位置和功能
        /// </summary>
    public const string GuideGetPositionFunction= "GuideGetPositionFunction";
        /// <summary>
        /// 解锁新功能
        /// </summary>
    public const string UnlockNewFunction= "UnlockNewFunction";
        /// <summary>
        /// 改变难度
        /// </summary>
    public const string ChangeHard= "ChangeHard";
        /// <summary>
        /// 退出登录
        /// </summary>
    public const string Logout= "Logout";
}
