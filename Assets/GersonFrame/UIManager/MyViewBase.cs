using UnityEngine;
using DG.Tweening;
using UnityEngine.UI;
using System;

namespace GersonFrame
{
    public class MyViewBase : MonoBehaviourSimplify
    {


        protected float m_showAmTime = 200;

        private void Awake()
        {
            this.InitView();
        }

        protected virtual void InitView()
        {
        }


        public virtual void OnEnter(object param = null)
        {
            this.gameObject.SetActive(true);
            this.ShowScalAm();
        }


        public virtual void OnPause()
        {
        }


        public virtual void OnExit()
        {
            this.HideScalAm();
        }

        public virtual void OnResume()
        {

        }

        public virtual void ShowScalAm()
        {
            transform.DOScale(Vector3.zero, 0.3f).From().onComplete = ShowEnd;
        }

        public virtual void HideScalAm()
        {
            transform.DOScale(Vector3.zero, 0.3f).onComplete = HideEnd;
        }

        protected virtual void ShowEnd()
        {
        }

        protected virtual void HideEnd()
        {
            this.gameObject.Hide();
            transform.localScale = Vector3.one;
        }

     

        protected override void OnBeforeDestroy()
        {

        }
    }




}