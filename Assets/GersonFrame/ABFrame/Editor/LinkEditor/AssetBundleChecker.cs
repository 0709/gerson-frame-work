﻿using UnityEditor;
using UnityEngine;

public class AssetBundleChecker
{
    [MenuItem("Tools/CreateLink.xml")]
    public static void Create()
    {
        var generator = new LinkXmlGenerator();

        generator.SetTypeConversion(typeof(UnityEditor.Animations.AnimatorController), typeof(RuntimeAnimatorController));

        foreach (var bundleName in AssetDatabase.GetAllAssetBundleNames())
        {
            var assetPaths = AssetDatabase.GetAssetPathsFromAssetBundle(bundleName);
            generator.AddAssets(assetPaths);
        }
        generator.Save("Assets/link.xml");
        Debug.Log("link 文件构建完毕");
        AssetDatabase.Refresh();
    }
}